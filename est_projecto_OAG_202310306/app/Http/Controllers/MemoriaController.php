<?php
//Creamos un nuevo controllador 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Con el nombre de nuestro programa que sera memoria con la extension del controllador 
class MemoriaController extends Controller
{
    //Crearemos una nueva funcion llamada memoria 
    public function Memoria(){
        $resultado = '';
        //Crearemos una variable llamada resultado donde guardaremos el resultado y usaremos  el memory get usage para saber la cantidad
        //de memoria que estamos usando y usaremos el str_repeat para repetir 50 veces y darnos cuenta el uso de  la  memoria al hacer esa accion
        $resultado.= "Memoria en uso: ". memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024) .') ';
        $resultado.= str_repeat("a", 50);
         $resultado.="Memoria en uso: ". memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024). ') ';
         $resultado.='Memoria limite: ' . ini_get('memory_limit');
         //y usamos el ini_get para que nos de el limite de memoria que tenemos y  usamos un return para imprimir los valores guardados en resultado 
        return  view('memoria',['resultado'=>$resultado]);
    }
}
?>