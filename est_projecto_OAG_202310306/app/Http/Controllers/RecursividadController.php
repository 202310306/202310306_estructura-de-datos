<?php
//igual crearemos un controllador lo cual nos pondra esto como predeterminado para ser un controlador
namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Creamos una clase llamada RecursividadController que se  extienda con los terminos del controller
class RecursividadController extends Controller
{
    //Crearemos una funcion publica llamada recursividad con la variable $Nfin , $N 
    //Con el cual  pondremos un conteo con el que sumaremos 1 cada vez que sea usado en la variable $N
    public function recursividad($Nfin, $N){
        $N += 1;
        if($N<=$Nfin){
            echo $N, '<br>';
            $this->recursividad($Nfin, $N);
        }
    }
    //Ahora crearemos una funcion llamada incrementable con  la cual pondremos el valor en el cual queremos que imprima jaja mira un 14
    public function Incrementable(){
        //el this lo usaremos para que imprima desde el 13 guardada en recursividad 
        $this->recursividad(13, 0);
        if($N=14){
            echo $N, " Jaja mira un 14", '<br>';
        }
        //Y por ultimo  le ponemos que cuando el variable $N sea igual a 50 imprima esto ya se acabo que fue lo que pusimos como echo
        $this->recursividad(50, 14);
        if($N=50){
            echo $N, "Esto ya se acabo", "<br>";
    }
}}