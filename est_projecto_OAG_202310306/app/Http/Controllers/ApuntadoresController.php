<?php
//Creamos un controllador  nuevo 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Creamos una clase llamada ApuntadoresController que se extienda con las caracteristicas del Controller
class ApuntadoresController extends Controller
{
    //Crearemos una funcion llamada inicio que es donde guardaremos todo
    public function Inicio(){
        //La edad la pondremos con un valor establecido en este caso 26 y pondremos que returne el view que creamos de html 
        //con el nombre apuntadores para que tenga curepo de pagina y se imprima $Aedad que lo pusimos como que tambien su valor era  26  
        $edad = 26;
        $Aedad = & $edad;
        unset($edad);
        return view("apuntadores",["Aedad"=>$Aedad]);
    }
}