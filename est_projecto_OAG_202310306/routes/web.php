<?php
//aqui ponemos los controlladores que tenemos para  poder usarlos  y mandarlos a enviar con su ruta correspondiende dandole la dirrecion de donde esta 
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RecursividadController;
use App\Http\Controllers\ApuntadoresController;
use App\Http\Controllers\MemoriaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//aqui  pondremos las rutas para poder acceder a ellas desde el  sitio web
Route::get('/recursividad', [RecursividadController:: class, 'Incrementable']);
Route::get('/apuntadores', [ApuntadoresController:: class, 'Inicio']);
Route::get('/memoria', [MemoriaController:: class, 'Memoria']);